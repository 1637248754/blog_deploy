#! /bin/bash

sleep 10

docker logs -f blog_web_1 &

while [[ `docker logs --tail 1 blog_web_1` != *'- Worker 0 (PID:'* ]]
do
  sleep 1
done

kill %1
