#! ruby

require 'yaml'

class Version
  MAX_NUM = 99

  def initialize(evn = 'production')
    @env = evn
    @docker_image_version = YAML.load_file("#{__dir__}/docker_image_version.yml")
    @version = @docker_image_version[@env]
  end

  def versions
    @version['version'].split('.').map(&:to_i)
  end

  # next version
  def next_version
    step = 1
    versions.reverse.map do |segment|
      if segment == MAX_NUM && step.positive?
        segment = 0
      elsif segment < MAX_NUM
        segment += step
        step = 0
      end
      segment
    end.reverse.join('.')
  end

  # current version
  def current_version
    @version['version']
  end

  def current_version=(version)
    @version['version'] = version
    File.open("#{__dir__}/docker_image_version.yml", 'w') do |f|
      f.write @docker_image_version.to_yaml
      f.close
    end
  end
end
